# 项目介绍
本项目采用react + ES6 + ant-design实现，其中jwps.es.js是由WPS开发组特别提供，本项目对该js源码做了部分调整

1. react-roter-dom 5.x
2. react-dom
3. ant
4. react-hot-loader
5. axios
6. react-scripts


## 特别注意
1. 次前端工程必须配合后台部分使用,后台代码地址 https://gitee.com/mose-x/wps-view-java.git
2. api中index.jsx中的axios.defaults.baseURL请自行更换，必须和[wps开放平台](https://open.wps.cn/weboffice)上的回调URL一致
3. 其它事项请参考后台部分readme.md

## 运行项目说明
1. 先申请wps在线编辑服务相关appkey等
2. 配置后台wps回调以及阿里云oss/七牛，mysql等
3. 部署后台服务
4. 按照以下步骤开始运行前台
5. 运行图就不放了，详见以下演示地址

## 安装依赖
```
yarn install
```

### 开发运行
```
yarn start
```

### 生产打包
```
yarn build
```

### 演示地址
https://ljserver.cn/wpsonline-react