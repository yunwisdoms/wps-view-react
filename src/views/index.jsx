import React from "react"
import {Link} from "react-router-dom"

export default class Index extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            color1:{
                color:'#42b983'
            },
            color2:{
                color:'#2c3e50'
            }
        }
    }

    componentDidMount() {
        this.changeColor()
    }

    changeColor() {
        const pathname = window.location.href;
        if (pathname.search('webFile') !== -1){
            this.wColor()
        }
        if (pathname.search('dbFile') !== -1){
            this.dColor()
        }
    };

    wColor = () =>{
        this.setState({
            color1:{
                color:'#42b983'
            },
            color2:{
                color:'#2c3e50'
            }
        })
    };

    dColor = () =>{
        this.setState({
            color1:{
                color:'#2c3e50'
            },
            color2:{
                color:'#42b983'
            }
        })
    };

    render(){
        return(
            <div id={"nav"}>
                <h3>
                    <Link to="/webFile" style={this.state.color1} onClick={()=>{
                        this.wColor()
                    }}>URL在线文档预览</Link>
                    &nbsp;|&nbsp;
                    <Link to="/dbFile" style={this.state.color2} onClick={()=>{
                        this.dColor()
                    }}>数据库文档预览</Link>
                </h3>
                {this.props.children}
            </div>
        )
    }
}
