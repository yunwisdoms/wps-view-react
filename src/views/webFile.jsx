import React from "react"
import {Input, Button, message,Spin} from 'antd';
import {DeleteOutlined,CheckOutlined} from '@ant-design/icons';
import {getViewUrlWebPath} from "../api/index"
import {checkUrl} from "../confun/index"
const { TextArea } = Input;

export default class WebFilePage extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            textAreaValue: '',
            loading:false
        };

        this.clClean = this.clClean.bind(this);
        this.clSubmit = this.clSubmit.bind(this);
        this.textAreaChange = this.textAreaChange.bind(this);
    }

    render() {
        return <div id={'webFile'}>
            <h1 className={'w-title'}>请输入可访问的FileUrl地址</h1>
            <div style={{margin:'30px 0'}}/>
            <TextArea
                placeholder="请输入内容，保证文档可访问，如http://www.file.cn/a.docx"
                onChange={this.textAreaChange}
                value={this.state.textAreaValue}
                autoSize={{minRows: 3, maxRows: 5}}
                className={'w-text'}
            />
            <Spin spinning={this.state.loading}>
                <div style={{margin:'40px 0'}} />
                <br/>
                <Button
                    className={'w-btClass'}
                    icon={<DeleteOutlined />}
                    onClick={this.clClean}
                >
                    clean
                </Button>
                <Button
                    className={'w-btClass'}
                    icon={<CheckOutlined />}
                    onClick={this.clSubmit}
                >
                    view
                </Button>
            </Spin>
        </div>
    }

    clClean = ()=> {
        this.setState({
            textAreaValue: ''
        })
    };

    clSubmit = ()=> {
        const par = this.state.textAreaValue;
        if (checkUrl(par)){
            this.getViewUrl(par)
        }
    };

    textAreaChange = ({ target: { value } })=>{
        this.setState({
            textAreaValue: value
        })
    };

    getViewUrl = (fileUrl)=>{
        this.openLoading();
        getViewUrlWebPath({fileUrl}).then((res)=>{
            const r = res.data.data;
            sessionStorage.wpsUrl = r.wpsUrl;
            sessionStorage.token = r.token;
            this.jumpTo();
            this.cleanLoading();
        }).catch(()=>{
            message.error("请求异常");
            this.cleanLoading();
        })
    };

    jumpTo = ()=>{
        // 开启了hash模式，注意这里的 #，生不如死
        window.open('#/viewFile','_blank');
    };

    openLoading = ()=>{
        this.setState({
            loading: true
        });
    };

    cleanLoading = ()=>{
        this.setState({
            loading: false
        });
    };
}
