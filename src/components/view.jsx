import React from "react"
import * as WPS from '../static/jwps.es'

export default class ViewFilePage extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            simpleMode: false,
            url: sessionStorage.wpsUrl,
            token: sessionStorage.token
        }
    }

    componentDidMount() {
        this.openWps(this.state.url,this.state.token);
    }

    openWps = (url, token) => {
        const wps = WPS.config({
            mode: this.state.simpleMode?'simple':'normal',
            mount: document.querySelector('#root'),
            wpsUrl: url,
        });
        wps.setToken({token});
        let app = wps.Application;
        console.log(JSON.stringify(app))
    };

    render() {
        return <div />
    }

}
